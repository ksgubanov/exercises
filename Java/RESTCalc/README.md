Runing the app
-------
With maven:
`mvn spring-boot:run`

Default port is 8081. To change it add `-Dspring-boot.run.arguments=--server.port=<port>` 
