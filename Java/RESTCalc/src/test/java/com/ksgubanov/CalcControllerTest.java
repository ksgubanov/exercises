package com.ksgubanov;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CalcControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void calcHappyFlowAdd() throws Exception {
        this.mockMvc.perform(
                get("/calc")
                        .param("op", CalcOperations.ADD.name())
                        .param("op_l", "3")
                        .param("op_r", "4"))
                .andExpect(status().isOk())
                .andExpect(content().string(Matchers.containsString("7")));
    }

    @Test
    void calcHappyFlowSub() throws Exception {
        this.mockMvc.perform(
                get("/calc")
                        .param("op", CalcOperations.SUB.name())
                        .param("op_l", "33")
                        .param("op_r", "44"))
                .andExpect(status().isOk())
                .andExpect(content().string(Matchers.containsString("-11")));
    }

    @Test
    void calcHappyFlowMult() throws Exception {
        this.mockMvc.perform(
                get("/calc")
                        .param("op", CalcOperations.MULT.name())
                        .param("op_l", "3")
                        .param("op_r", "4"))
                .andExpect(status().isOk())
                .andExpect(content().string(Matchers.containsString("12")));
    }

    @Test
    void calcHappyFlowDiv() throws Exception {
        this.mockMvc.perform(
                get("/calc")
                        .param("op", CalcOperations.DIV.name())
                        .param("op_l", "12")
                        .param("op_r", "4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(Matchers.containsString("3.0")));
    }

    @Test
    void sqrtHappyFlow() throws Exception {
        this.mockMvc.perform(
                get("/calc/sqrt")
                        .param("operand", "81"))
                .andExpect(status().isOk())
                .andExpect(content().string(Matchers.containsString("9")));
    }
}