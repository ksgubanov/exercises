package com.ksgubanov;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calc")
public class CalcController {

    @GetMapping(value = "*")
    @ResponseBody
    public String defaultResponse() {
        return "There is no such endpoint";
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Float> calc(@RequestParam(value = "op") CalcOperations operation,
                               @RequestParam(value = "op_l") float op_l,
                               @RequestParam(value = "op_r") float op_r) {
        Float result = null;
        HttpStatus statusCode = HttpStatus.OK;
        switch (operation) {
            case ADD:   result = op_l + op_r; break;
            case SUB:   result = op_l - op_r; break;
            case MULT:  result = op_l * op_r; break;
            case DIV:   result = op_l / op_r; break;
            default: statusCode = HttpStatus.I_AM_A_TEAPOT;
        }
        return new ResponseEntity<>(result, statusCode);
    }

    @GetMapping("/sqrt")
    @ResponseBody
    public double getSqrt(@RequestParam(value = "operand") double operand) {
        return Math.sqrt(operand);
    }
}
