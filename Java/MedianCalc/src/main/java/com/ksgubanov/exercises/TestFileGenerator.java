package com.ksgubanov.exercises;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import static java.lang.System.currentTimeMillis;

public class TestFileGenerator {
    private static int numOfDoubles = 50000000;

    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        Random rnd = new Random();

        try (FileWriter fw = new FileWriter(System.getProperty("user.home")+"/bigfile.txt")) {
            rnd.doubles(numOfDoubles).forEach(d -> {
                double doubleVal = d * rnd.nextInt(100);
                if (rnd.nextInt(numOfDoubles / 100) == 0) {
                    doubleVal = -doubleVal;
                }
                String stringVal = String.format("%.5f", doubleVal);
                if (rnd.nextInt(numOfDoubles / 100) == 0) {
                    stringVal = stringVal.substring(0, stringVal.indexOf("."));
                }
                if (rnd.nextInt(numOfDoubles / 100) == 0) {
                    stringVal = stringVal.replace(".", ",");
                }
                if (rnd.nextInt(numOfDoubles / 10) == 0) {
                    stringVal = "0";
                }
                try {
                    fw.write(stringVal);
                    fw.write(System.lineSeparator());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        float endTime = currentTimeMillis() - startTime;
        System.out.println("Finished in " + endTime / 1000F + " seconds");
    }
}
