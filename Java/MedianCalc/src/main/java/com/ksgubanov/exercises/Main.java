package com.ksgubanov.exercises;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.System.currentTimeMillis;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length == 0 || args[0].isEmpty()) {
            System.out.println("Please provide the filename as argument");
            return;
        }

        if (Files.notExists(Path.of(args[0]))) {
            System.out.println("File not found");
            return;
        }
        long startTime = currentTimeMillis();
        String median = findMedian(100, args[0]);
        float endTime = currentTimeMillis() - startTime;
        System.out.println("Finished in " +  endTime/1000F + " seconds");
        System.out.println("The median is " +  median);
    }

    public static String findMedian(int chunkSize, String pathToFile) throws IOException {
        ChunksManager cm = new ChunksManager(chunkSize);
        List<Integer> chunks = new LinkedList<>();

        long startTime = System.currentTimeMillis();

        readFileIntoTmpFileChunks(cm, chunks, pathToFile);
        File mergedTmpFile = File.createTempFile("merged",
                null, new File("."));
        mergedTmpFile.deleteOnExit();
        OutputStream out = new FileOutputStream(mergedTmpFile);
        BufferedWriter fbw = new BufferedWriter(new OutputStreamWriter(out));

        long numberOfNumbers = mergeSortSortedChunks(fbw, cm, chunks);

        String line = getMedianFromMergedSortedFile(mergedTmpFile, numberOfNumbers);

        if (numberOfNumbers % 2 == 0) {
            System.out.println("Calculated as average of element " + numberOfNumbers/2 + " and element " + (numberOfNumbers/2+1));
        }
        return line;
    }


    /**
     * Gets the middle value from the file if it has odd number or values
     * or calculated the average value of two middle values if the number of
     * values is even.
     * @param mergedFile A file with sorted values to get median from
     * @param numberOfNumbers Pre-counted number of values in the file
     * @return Median of the values in file
     */
    private static String getMedianFromMergedSortedFile(File mergedFile, long numberOfNumbers) throws IOException {
        if (numberOfNumbers % 2 == 1) { //odd
            try (Stream<String> lines = Files.lines(mergedFile.toPath())) {
                return lines.skip(numberOfNumbers/2).findFirst().get();
            }
        }
        else { //even
            try (Stream<String> lines = Files.lines(mergedFile.toPath())) {
                Object[] midNumbers = lines.skip(numberOfNumbers/2-1).limit(2).toArray();
                return Double.toString((Double.parseDouble((String)midNumbers[0])+Double.parseDouble((String)midNumbers[1]))/2d);
            }
        }
    }

    /**
     * Performs merge sort of chunks stored as temporary files
     * NOTE: it still stores the value in the memory, which has to be replaced with disk storage
     * to be able to process big files.
     * @param bufferedWriter Writer for the resulting file
     * @param cm ChunkManager instance containing references to the tmp files
     * @param sortedChunks List of indices of sorted chunks
     * @return number of lines (values) in the resulted file
     */
    private static long mergeSortSortedChunks(BufferedWriter bufferedWriter, ChunksManager cm, List<Integer> sortedChunks) throws IOException {
        Comparator<LinkedList<Double>> cmp = Comparator.comparing(LinkedList::peek);
        PriorityQueue<LinkedList<Double>> pq = new PriorityQueue<>(10, cmp);

        for (int chunkIndex : sortedChunks) {
            LinkedList<Double> valuesBuffer = cm.getChunk(chunkIndex);
            if (!valuesBuffer.isEmpty()) {
                pq.add(valuesBuffer);
            }
        }

        long lineCounter = 0;

        try (bufferedWriter) {
            while (pq.size() > 0) {
                LinkedList<Double> valuesBuffer = pq.poll();
                Double r = valuesBuffer.poll();
                bufferedWriter.write(r.toString());
                bufferedWriter.newLine();
                ++lineCounter;
                if (!valuesBuffer.isEmpty()) {
                    pq.add(valuesBuffer); // add it back
                }
            }
        }
        return lineCounter;
    }

    /**
     * Reads input file, divides it into sorted pieces and stores them as tmp files
     * which have to be deleted upon program termination
     * The number of values in the files are defined by {@code ChunksManager.CHUNK_PART_SIZE}
     * @param cm ChunksManager instance to store files.
     * @param chunks List of chunks' IDs to be filled.
     * @param fileName Name of the input file
     * @return
     */
    private static long readFileIntoTmpFileChunks(ChunksManager cm, List<Integer> chunks, String fileName) {
        long lineCounter = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            List<Double> doubleValues = new LinkedList<>();

            while ((line = br.readLine()) != null)
            {
                if (doubleValues.size() == cm.getChunkPartSize()) {
                    doubleValues.sort(Comparator.naturalOrder());
                    chunks.add(cm.addChunk(doubleValues));
                    doubleValues.clear(); // TODO: create a new one is faster?
                }

                doubleValues.add(parseDoubleFromString(line));
                lineCounter++;
            }
            if (!doubleValues.isEmpty()) {
                doubleValues.sort(Comparator.naturalOrder());
                chunks.add(cm.addChunk(doubleValues));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lineCounter;
    }

    /**
     * Slightly customized double to string parsing. Replaces "," with "."
     * @param line
     * @return Parsed double value of string.
     */
    private static double parseDoubleFromString(String line) {
        line = line.replace(",",".");
        return Double.parseDouble(line);
    }

}
