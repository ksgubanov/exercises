package com.ksgubanov.exercises;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;

public class ChunksManager {
    // 2147483647 values (files) max, so 1000 bytes per file (125 doubles) give us around 2TB
    // have to be extended if it's not enough (input data is too big and big number of numbers in
    // one chunk will significantly decrease performance
    private List<File> tmpFiles = new LinkedList<>();
    private int fileCounter = 0;
    private static File tmpDir = new File("./.tmp/");
    private static short sizeOfDoubleBytes = Double.SIZE/8;
    private final int CHUNK_PART_SIZE;

    // CONSTRUCTORS

    public ChunksManager(int chunkSize) {
        if(chunkSize <= 0) {
            throw new NumberFormatException("Chunk size must me bigger than zero");
        }
        CHUNK_PART_SIZE = chunkSize;
    }

    // GETTERS SETTERS

    public int getChunkPartSize() {
        return CHUNK_PART_SIZE;
    }

    /**
     * Creates a tmp file, writes the values from the provided list
     * and marks the file to be deleted on exit.
     * @param chunk Data to be written to the tmp file
     * @return Index of added chunk
     */
    public int addChunk(List<Double> chunk) {
        if (chunk == null || chunk.isEmpty()) {
            return -1;
        }
        try {
            File f = File.createTempFile("mediancalc_", null);
            f.deleteOnExit();
            writeFile(chunk, f);
            tmpFiles.add(f);
            return tmpFiles.size() - 1; //index of last element
        } catch (Exception e) {
            System.out.println("Error occured while writing temporary file");
            return -1;
        }
    }

    public void deleteAllChunks() {
        for (int chunkId=0; chunkId < tmpFiles.size(); chunkId++) {
            deleteChunk(chunkId);
        }
    }

    /**
     * Returns list of values stored in the chunk with provided ID
     * @param chunkId
     * @return
     */
    public LinkedList<Double> getChunk(int chunkId) {
        LinkedList<Double> valuesFromFile = new LinkedList<>();
        try {
            File f = tmpFiles.get(chunkId);
            BufferedReader br = new BufferedReader(new FileReader(f));

            br.lines().forEach(s -> valuesFromFile.add(Double.parseDouble(s)));
        } catch (Exception ex) {
            return null;
        }
        return valuesFromFile;
    }

    /**
     * Writes data to already existing file
     * @param chunkId
     * @param data
     * @return
     * @throws Exception
     */

    public boolean updateChunk(int chunkId, List<Double> data) {
        try {
            File f = tmpFiles.get(chunkId);
            writeFile(data, f, false);
        } catch (Exception ex) { //more concrete exception
            return false;
        }
        return true;
    }

    /**
     * Deletes temporary file containing the chunk with chunkId
     * @param chunkId ID of chunk to be deleted
     * @return true if file existed and successfully deleted, otherwise false
     */
    public boolean deleteChunk(int chunkId) {
        Optional<File> file = Optional.ofNullable(tmpFiles.get(chunkId));
        if (file.isEmpty()) {
            return false;
        }
        tmpFiles.remove(chunkId);
        return file.get().delete();
    }

    // PRIVATE METHODS

    /**
     * Creates a new file (if doesn't exist) and write the chunk of doubles as byte arrays
     * @param chunk
     * @param f
     * @throws IOException
     */
    private void writeFile(List<Double> chunk, File f, boolean append) throws IOException {
        FileWriter fw = new FileWriter(f, append);
        for (Double d : chunk) {
            fw.write(d.toString()+System.lineSeparator());
        }
        fw.flush();
    }

    private void writeFile(List<Double> chunk, File f) throws IOException {
        writeFile(chunk, f, true);
    }

    private LinkedList<Double> byteArrayToListOfDoubles(byte[] bytes) {
        LinkedList<Double> numbers = new LinkedList<>();
        for (int b=0; b<bytes.length; b+= sizeOfDoubleBytes) {
            numbers.add(toDouble(Arrays.copyOfRange(bytes, b, b + sizeOfDoubleBytes)));
        }
        return numbers;
    }

    private static byte[] toByteArray(double value) {
        byte[] bytes = new byte[sizeOfDoubleBytes];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    private static double toDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }
}
