package com.ksgubanov.exercises;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

class ChunksManagerTest {

    private static final int CHUNK_SIZE = 10;
    private ChunksManager unitUnderTest;

    @BeforeEach
    void setUp() {
        this.unitUnderTest = new ChunksManager(CHUNK_SIZE);
    }

    @AfterEach
    void tearDown() {
        unitUnderTest.deleteAllChunks();
    }

    @Test
    public void whenConstructorGivenNegativeValue_thenExceptionThrown() {
        assertThrows(NumberFormatException.class, () -> new ChunksManager(-1));
    }

    @Test
    public void whenGetChunkPartSize_thenSetPreviouslyPartSizeReturned() {
        assertEquals(CHUNK_SIZE, unitUnderTest.getChunkPartSize());
    }

    @Test
    public void whenAddChunkPassedCorrectValue_thenCanGetChunkReturnsTheSame() {
        List<Double> testList = List.of(
                -11.33d,
                0d,
                333.1111111111111d,
                Double.MAX_VALUE,
                Double.NEGATIVE_INFINITY,
                Double.NaN);
        int chunkId = unitUnderTest.addChunk(testList);

        assertTrue(chunkId >= 0);
        assertEquals(testList.size(), unitUnderTest.getChunk(chunkId).size());
        assertEquals(testList, unitUnderTest.getChunk(chunkId));
    }

    @Test
    public void whenAddChunkNullPassed_thenNegativeOneReturned() {
        assertEquals(-1, unitUnderTest.addChunk(null));
    }

    @Test
    public void WhenAddChunkEmptyListPassed_thenNegativeOneReturned() {
        assertEquals(-1, unitUnderTest.addChunk(Collections.emptyList()));
    }

    @Test
    void WhenGettingNotExistingChunk_thenReturnedNull() {
        unitUnderTest.addChunk(List.of(1.0d, 1.d));
        assertNull(unitUnderTest.getChunk(999));
    }

    @Test
    void WhenUpdateChunk_thenCanGetChunkAndCompareValues() {
        List<Double> testList = new ArrayList<>(List.of(1.11d, 1.d));
        int chunkId = unitUnderTest.addChunk(testList);
        assertEquals(testList, unitUnderTest.getChunk(chunkId));
        testList.add(433.375d);
        assertTrue(unitUnderTest.updateChunk(chunkId, testList));
        assertEquals(testList, unitUnderTest.getChunk(chunkId));
    }

    @Test
    void whenDeleteChunk_thenGettingThisChunkWillReturnNull() {
        List<Double> testList = List.of(1.11d, 1.d);
        int chunkId = unitUnderTest.addChunk(testList);
        assertEquals(testList, unitUnderTest.getChunk(chunkId));
        assertTrue(unitUnderTest.deleteChunk(chunkId));
        assertNull(unitUnderTest.getChunk(chunkId));
    }

    @Test
    @Ignore("Should we test something else?")
    public void yetAnotherTest() {
        fail();
    }
}